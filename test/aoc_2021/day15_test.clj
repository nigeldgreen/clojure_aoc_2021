(ns aoc-2021.day15-test
  (:require [clojure.test :as ct]
            [aoc-2021.fileio :as io]
            [aoc-2021.day15 :as d15]
            [clojure.string :as str]))

(ct/deftest can-test-path-membership
  (ct/is (= true (d15/in-path? 0 [0 1 2 3 4])))
  (ct/is (= true (d15/in-path? 2 [0 1 2 3 4])))
  (ct/is (= false (d15/in-path? "test" [0 1 2 3 4])))
  (ct/is (= false (d15/in-path? 42 [0 1 2 3 4]))))

(ct/deftest can-test-path-is-at-end
  (ct/is (= true (d15/is-end? 0 [0 1 2 3 4])))
  (ct/is (= true (d15/is-end? 2 [0 1 2 3 4])))
  (ct/is (= false (d15/is-end? "test" [0 1 2 3 4])))
  (ct/is (= false (d15/is-end? 42 [0 1 2 3 4]))))
