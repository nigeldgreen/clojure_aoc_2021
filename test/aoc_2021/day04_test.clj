(ns aoc-2021.day04-test
  (:require [clojure.test :as ct]
            [aoc-2021.day04 :as d4]
            [aoc-2021.fileio :refer [read-file-to-list]]))

(def input (read-file-to-list "day_04_test.txt"))
(def first-card (first (d4/get-cards input)))


(def call-nums '(7 4 9 5 11 17 23 2 0 14 21 24 10 16 13 6 15 25 12 22 18 20 8 19 3 26 1))
(ct/deftest can-get-correct-call-nums
  (ct/is (= call-nums (d4/get-call-nums input)))
)


(def b-cards '((22 13 17 11 0 8 2 23 4 24 21 9 14 16 7 6 10 3 18 5 1 12 20 15 19)
               (3 15 0 2 22 9 18 13 17 5 19 8 7 25 23 20 11 10 24 4 14 21 16 12 6)
               (14 21 17 24 4 10 16 15 9 19 18 8 23 26 20 22 11 13 6 5 2 0 12 3 7)))
(ct/deftest get-line-and-col-tests
  (ct/is (= b-cards (d4/get-cards input)))

  (ct/is (= '((22 13 17 11 0) (8 2 23 4 24) (21 9 14 16 7) (6 10 3 18 5) (1 12 20 15 19))
             (d4/get-lines first-card)))

  (ct/is (= '((22 8 21 6 1) (13 2 9 10 12) (17 23 14 3 20) (11 4 16 18 15) (0 24 7 5 19))
             (d4/get-cols first-card))))
(ct/deftest check-winning-cards
  (ct/is (= nil
            (d4/winner? '(22 13 17 11 0 8 2 23 4 24 21
                           9 14 16 7 6 10 3 18 5 1 12 20 15 19))))

  (ct/is (= nil
            (d4/winner? '(22 13 17 \x 0 8 2 23 4 24 21
                           9 14 \x \x \x \x \x 18 5 1 12 20 15 19))))

  (ct/is (= true
            (d4/winner? '(\x \x \x \x \x 8 2 23 4
                           24 21 9 14 16 7 6 10 3 18 5 1 12 20 15 19))))

  (ct/is (= true
            (d4/winner? '(\x 13 17 11 0 \x 2 23 4 24
                           \x 9 14 16 7 \x 10 3 18 5 \x 12 20 15 19)))))


(def test-card '(22 13 17 11 0 8 2 23 4 24 21 9 14 16 7 6 10 3 18 5 1 12 20 15 19))
(ct/deftest test-calling-numbers
  (ct/is (= '(22 13 \x 11 0 8 2 23 4 24 21 9 14 16 7 6 10 3 18 5 1 12 20 15 19)
            (d4/mark-card test-card 17))))


(def b-cards-start '((22 13 17 11 0 8 2 23 4 24 21 9 14 16 7 6 10 3 18 5 1 12 20 15 19)
                     (3 15 0 2 22 9 18 13 17 5 19 8 7 25 23 20 11 10 24 4 14 21 16 12 6)
                     (14 21 17 24 4 10 16 15 9 19 18 8 23 26 20 22 11 13 6 5 2 0 12 3 7)))
(def b-cards-after-1st-call '((22 13 17 \x 0 8 2 23 4 24 21 9 14 16 7 6 10 3 18 5 1 12 20 15 19)
                              (3 15 0 2 22 9 18 13 17 5 19 8 7 25 23 20 \x 10 24 4 14 21 16 12 6)
                              (14 21 17 24 4 10 16 15 9 19 18 8 23 26 20 22 \x 13 6 5 2 0 12 3 7)))
(def b-cards-after-4th-call '((\x 13 17 \x 0 8 2 23 \x 24 21 9 14 16 7 6 10 3 18 5 1 \x 20 15 19)
                              (3 15 0 2 \x 9 18 13 17 5 19 8 7 25 23 20 \x 10 24 \x 14 21 16 \x 6)
                              (14 21 17 24 \x 10 16 15 9 19 18 8 23 26 20 \x \x 13 6 5 2 0 \x 3 7)))
(ct/deftest test-check-call-updates-cards-correctly
  (ct/is (= b-cards-after-1st-call
            (d4/make-call b-cards-start 11)))
  (ct/is (= b-cards-after-4th-call
            (-> b-cards-start (d4/make-call 11) (d4/make-call 22) (d4/make-call 4) (d4/make-call 12))))
)


(def b-cards-no-winner '((22 13 17 11 0 8 2 23 4 24 21 9 14 16 7 6 10 3 18 5 1 12 20 15 19)
                         (3 15 0 2 22 9 18 13 17 5 19 8 7 25 23 20 11 10 24 4 14 21 16 12 6)
                         (14 21 17 24 4 10 16 15 9 19 18 8 23 26 20 22 11 13 6 5 2 0 12 3 7)))
(def b-cards-with-winner '((22 13 17 11 0 8 2 23 4 24 21 9 14 16 7 6 10 3 18 5 1 12 20 15 19)
                           (3 15 0 2 22 \x \x \x \x \x 19 8 7 25 23 20 11 10 24 4 14 21 16 12 6)
                           (14 21 17 24 4 10 16 15 9 19 18 8 23 26 20 22 11 13 6 5 2 0 12 3 7)))
(ct/deftest test-check-for-winner
  (ct/is (= nil
            (d4/check-cards b-cards-no-winner)))
  (ct/is (= '(3 15 0 2 22 \x \x \x \x \x 19 8 7 25 23 20 11 10 24 4 14 21 16 12 6)
            (d4/check-cards b-cards-with-winner)))
)


(def winning-card1 '(\x \x \x \x \x 10 16 15 \x 19 18 8 \x 26 20 22 \x 13 6 \x \x \x 12 3 \x))
(ct/deftest test-get-correct-map-at-end-of-part-one
  (ct/is (= [winning-card1 24]
            (d4/play-part-one (d4/get-call-nums input) b-cards)))
)
(ct/deftest test-value-of-winning-card-in-game-one
  (ct/is (= 4512
            (d4/winning-value [winning-card1 24])))
)


(def winning-card2 '(3 15 \x \x 22 \x 18 \x \x \x 19 8 \x 25 \x 20 \x \x \x \x \x \x \x 12 6))
(ct/deftest test-get-correct-map-at-end-of-part-two
  (ct/is (= [winning-card2 13]
            (d4/play-part-two (d4/get-call-nums input) b-cards)))
  )
(ct/deftest test-value-of-winning-card-in-game-two
  (ct/is (= 1924
            (d4/winning-value [winning-card2 13])))
)
