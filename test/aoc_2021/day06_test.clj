(ns aoc-2021.day06-test
  (:require [clojure.test :as ct]
            [aoc-2021.day06 :as d6]
            [aoc-2021.fileio :refer [read-file-to-list]]))

(ct/deftest can-get-correct-starting-map
  (ct/is (= {0 0, 1 1, 2 1, 3 2, 4 1, 5 0, 6 0, 7 0, 8 0} (d6/get-fish (read-file-to-list "day_06_test.txt"))))
)

(ct/deftest see-correct-fish-stats-over-time
  (ct/is (= {0 1, 1 1, 2 2, 3 1, 4 0, 5 0, 6 0, 7 0, 8 0} (into (sorted-map-by <) (d6/after-n-days 1 {0 0, 1 1, 2 1, 3 2, 4 1, 5 0, 6 0, 7 0, 8 0}))))
  (ct/is (= {0 1, 1 2, 2 1, 3 0, 4 0, 5 0, 6 1, 7 0, 8 1} (into (sorted-map-by <) (d6/after-n-days 2 {0 0, 1 1, 2 1, 3 2, 4 1, 5 0, 6 0, 7 0, 8 0}))))
  (ct/is (= {0 2, 1 1, 2 0, 3 0, 4 0, 5 1, 6 1, 7 1, 8 1} (into (sorted-map-by <) (d6/after-n-days 3 {0 0, 1 1, 2 1, 3 2, 4 1, 5 0, 6 0, 7 0, 8 0}))))
  (ct/is (= {0 1, 1 0, 2 0, 3 0, 4 1, 5 1, 6 3, 7 1, 8 2} (into (sorted-map-by <) (d6/after-n-days 4 {0 0, 1 1, 2 1, 3 2, 4 1, 5 0, 6 0, 7 0, 8 0}))))
  (ct/is (= 26 (reduce + (vals (d6/after-n-days 18 {0 0, 1 1, 2 1, 3 2, 4 1, 5 0, 6 0, 7 0, 8 0})))))
)

(ct/deftest answers-to-parts
  (ct/is (= 5934 (d6/part-one (read-file-to-list "day_06_test.txt"))))
  (ct/is (= 366057 (d6/part-one (read-file-to-list "day_06.txt"))))
  (ct/is (= 26984457539 (d6/part-two (read-file-to-list "day_06_test.txt"))))
  (ct/is (= 1653559299811 (d6/part-two (read-file-to-list "day_06.txt"))))
)