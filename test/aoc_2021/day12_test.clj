(ns aoc-2021.day12-test
  (:require [clojure.test :as ct]
            [aoc-2021.collection-as-list :as cal]
            [aoc-2021.day12 :as d12]))

(ct/deftest can-build-cave-system
  (let [system [{:name "d", :exits ["b"]}
                {:name "start", :exits ["b" "A"]}
                {:name "b", :exits ["d" "A" "end"]}
                {:name "A", :exits ["b" "end" "c"]}
                {:name "c", :exits ["A"]}]]
    (ct/is (= system (d12/parse-cave-system "day_12_test_10.txt")))))

(ct/deftest can-test-size-of-cave
  (ct/is (d12/is-small? "b"))
  (ct/is (d12/is-small? "nx"))
  (ct/is (not (d12/is-small? "A")))
  (ct/is (not (d12/is-small? "AGD"))))

(ct/deftest can-test-if-cave-visit-is-allowed
  (ct/is (d12/can-visit? ["start" "A" "d" "end"] "b"))
  (ct/is (d12/can-visit? ["start" "HN" "dc" "HN" "kj" "HN"] "end"))
  (ct/is (not (d12/can-visit? ["start" "A" "d" "end"] "d")))
  (ct/is (not (d12/can-visit? ["start" "dc" "LN" ] "dc"))))

(ct/deftest can-test-if-cave-visit-is-allowed-extended
  (ct/is (d12/can-visit-extended? ["start" "b" "d"] "b"))
  (ct/is (d12/can-visit-extended? ["start" "HN" "dc" "HN" "kj" "HN"] "dc"))
  (ct/is (d12/can-visit-extended? ["start" "A" "c" "A" "c" "A"] "b"))
  (ct/is (d12/can-visit-extended? ["start" "A" "c" "A" "c" "A" "b" "A"] "d"))
  (ct/is (not (d12/can-visit-extended? ["start" "dc" "LN" "dc" "LN"] "dc")))
  (ct/is (not (d12/can-visit-extended? ["start" "HN" "dc" "HN" "kj" "sa" "kj"] "sa")))
  (ct/is (not (d12/can-visit-extended? ["start" "A" "c" "A" "c" "A" "b" "A"] "b"))))

(ct/deftest can-get-new-journeys-for-existing-journey
  (let [caves (conj [] {:name "start", :exits ["A" "b"]}
                    {:name "A", :exits ["c" "b" "end"]}
                    {:name "c", :exits ["A"]}
                    {:name "b", :exits ["A" "d" "end"]}
                    {:name "d", :exits ["b"]}
                    {:name "end", :exits []})]
    (ct/is (= (d12/update-journey ["start" "A"] caves d12/can-visit?)
              [{:path ["start" "A" "c"], :state "active"}
               {:path ["start" "A" "b"], :state "active"}
               {:path ["start" "A" "end"], :state "complete"}]))
    (ct/is (= (d12/update-journey ["start" "A" "c" "A" "b"] caves d12/can-visit?)
              [{:path ["start" "A" "c" "A" "b" "A"], :state "active"}
               {:path ["start" "A" "c" "A" "b" "d"], :state "active"}
               {:path ["start" "A" "c" "A" "b" "end"], :state "complete"}]))
    (ct/is (= (d12/update-journey ["start" "A" "c" "A" "b" "d"] caves d12/can-visit?)
              [{:path ["start" "A" "c" "A" "b" "d" "b"], :state "invalid"}]))))

(ct/deftest can-get-part-one-test-answers
  (ct/is (= (d12/part-one "day_12_test_10.txt") 10))
  (ct/is (= (d12/part-one "day_12_test_19.txt") 19))
  (ct/is (= (d12/part-one "day_12_test_226.txt") 226)))

(ct/deftest can-get-part-two-test-answers
  (ct/is (= (d12/part-two "day_12_test_10.txt") 36))
  (ct/is (= (d12/part-two "day_12_test_19.txt") 103))
  (ct/is (= (d12/part-two "day_12_test_226.txt") 3509)))