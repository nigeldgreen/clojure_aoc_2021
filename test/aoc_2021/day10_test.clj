(ns aoc-2021.day10-test
  (:require [clojure.test :as ct]
            [aoc-2021.day10 :as d10]
            [aoc-2021.fileio :refer [read-file-to-list]]))

(ct/deftest answers-to-part-one
  (ct/is (= (d10/part-one (read-file-to-list "day_10_test.txt"))
            26397))
  (ct/is (= (d10/part-one (read-file-to-list "day_10.txt"))
            240123)))

(ct/deftest answers-to-part-two
  (ct/is (= (d10/part-two (read-file-to-list "day_10_test.txt")) 288957))
  (ct/is (= (d10/part-two (read-file-to-list "day_10.txt")) 3260812321)))