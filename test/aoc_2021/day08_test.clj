(ns aoc-2021.day08-test
  (:require [clojure.test :as ct]
            [aoc-2021.day08 :as d8]
            [aoc-2021.fileio :refer [read-file-to-list]]))

(ct/deftest answers-to-part-one
  (ct/is (= (d8/part-one (read-file-to-list "day_08_test.txt")) 26))
  (ct/is (= (d8/part-one (read-file-to-list "day_08.txt")) 272))
)

(ct/deftest answers-to-part-two
  (ct/is (= (d8/part-two (read-file-to-list "day_08_test.txt")) 61229))
  (ct/is (= (d8/part-two (read-file-to-list "day_08.txt")) 1007675))
)

