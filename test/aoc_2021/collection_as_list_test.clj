(ns aoc-2021.collection-as-list-test
  (:require [clojure.test :as ct]
            [aoc-2021.collection-as-list :as glc]))

(def input1 (glc/parse-collection "collection_test_1.txt"))
(def input2 (glc/parse-collection "collection_test_2.txt"))

(ct/deftest parse-collection-test
  (ct/is (= (glc/parse-collection "collection_test_1.txt")
            {:coll "123456789" :length 3}))
  (ct/is (= (glc/parse-collection "collection_test_2.txt")
            {:coll "abcdefghijklmnopqrstuvwxy" :length 5})))

(ct/deftest identify-first-column
  (ct/is (true? (glc/in-first-column? 0 input1)))
  (ct/is (true? (glc/in-first-column? 6 input1)))
  (ct/is (false? (glc/in-first-column? 1 input1)))
  (ct/is (false? (glc/in-first-column? 5 input1)))
  (ct/is (true? (glc/in-first-column? 5 input2)))
  (ct/is (true? (glc/in-first-column? 15 input2)))
  (ct/is (false? (glc/in-first-column? 3 input2)))
  (ct/is (false? (glc/in-first-column? 21 input2))))

(ct/deftest identify-last-column
  (ct/is (true? (glc/in-last-column? 2 input1)))
  (ct/is (true? (glc/in-last-column? 8 input1)))
  (ct/is (false? (glc/in-last-column? 1 input1)))
  (ct/is (false? (glc/in-last-column? 3 input1)))
  (ct/is (true? (glc/in-last-column? 4 input2)))
  (ct/is (true? (glc/in-last-column? 24 input2)))
  (ct/is (false? (glc/in-last-column? 3 input2)))
  (ct/is (false? (glc/in-last-column? 17 input2))))

(ct/deftest identify-first-row
  (ct/is (true? (glc/in-first-row? 0 input1)))
  (ct/is (true? (glc/in-first-row? 2 input1)))
  (ct/is (false? (glc/in-first-row? 6 input1)))
  (ct/is (false? (glc/in-first-row? 8 input1)))
  (ct/is (true? (glc/in-first-row? 0 input2)))
  (ct/is (true? (glc/in-first-row? 4 input2)))
  (ct/is (false? (glc/in-first-row? 5 input2)))
  (ct/is (false? (glc/in-first-row? 25 input2))))

(ct/deftest identify-last-row
  (ct/is (false? (glc/in-last-row? 0 input1)))
  (ct/is (true? (glc/in-last-row? 6 input1)))
  (ct/is (true? (glc/in-last-row? 8 input1)))
  (ct/is (false? (glc/in-last-row? 6 input2)))
  (ct/is (true? (glc/in-last-row? 21 input2)))
  (ct/is (true? (glc/in-last-row? 25 input2))))

(ct/deftest get-strict-adjacents-test
  (ct/is (= (glc/get-strict-adjacents input1 0) '(1 3)))
  (ct/is (= (glc/get-strict-adjacents input1 1) '(0 2 4)))
  (ct/is (= (glc/get-strict-adjacents input1 2) '(1 5)))
  (ct/is (= (glc/get-strict-adjacents input1 3) '(0 4 6)))
  (ct/is (= (glc/get-strict-adjacents input1 4) '(1 3 5 7)))
  (ct/is (= (glc/get-strict-adjacents input1 5) '(2 4 8)))
  (ct/is (= (glc/get-strict-adjacents input1 6) '(3 7)))
  (ct/is (= (glc/get-strict-adjacents input1 7) '(4 6 8)))
  (ct/is (= (glc/get-strict-adjacents input1 8) '(5 7)))

  (ct/is (= (glc/get-strict-adjacents input2 0) '(1 5)))
  (ct/is (= (glc/get-strict-adjacents input2 4) '(3 9)))
  (ct/is (= (glc/get-strict-adjacents input2 6) '(1 5 7 11)))
  (ct/is (= (glc/get-strict-adjacents input2 11) '(6 10 12 16)))
  (ct/is (= (glc/get-strict-adjacents input2 18) '(13 17 19 23)))
  (ct/is (= (glc/get-strict-adjacents input2 20) '(15 21))))

(ct/deftest get-all-adjacents-test
  (ct/is (= (glc/get-all-adjacents input1 0) '(1 3 4)))
  (ct/is (= (glc/get-all-adjacents input1 1) '(0 2 3 4 5)))
  (ct/is (= (glc/get-all-adjacents input1 2) '(1 4 5)))
  (ct/is (= (glc/get-all-adjacents input1 3) '(0 1 4 6 7)))
  (ct/is (= (glc/get-all-adjacents input1 4) '(0 1 2 3 5 6 7 8)))
  (ct/is (= (glc/get-all-adjacents input1 5) '(1 2 4 7 8)))
  (ct/is (= (glc/get-all-adjacents input1 6) '(3 4 7)))
  (ct/is (= (glc/get-all-adjacents input1 7) '(3 4 5 6 8)))
  (ct/is (= (glc/get-all-adjacents input1 8) '(4 5 7)))
  (ct/is (= (glc/get-all-adjacents input2 2) '(1 3 6 7 8)))
  (ct/is (= (glc/get-all-adjacents input2 4) '(3 8 9)))
  (ct/is (= (glc/get-all-adjacents input2 11) '(5 6 7 10 12 15 16 17)))
  (ct/is (= (glc/get-all-adjacents input2 15) '(10 11 16 20 21)))
  (ct/is (= (glc/get-all-adjacents input2 17) '(11 12 13 16 18 21 22 23)))
  (ct/is (= (glc/get-all-adjacents input2 21) '(15 16 17 20 22)))
  (ct/is (= (glc/get-all-adjacents input2 24) '(18 19 23))))
