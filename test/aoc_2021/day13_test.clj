(ns aoc-2021.day13-test
  (:require [clojure.test :as ct]
            [aoc-2021.fileio :as io]
            [aoc-2021.day13 :as d13]))

(ct/deftest test-break-apart-input
  (let [input (io/read-file-to-list "day_13_test.txt")
        coordinates '({:x 6, :y 10} {:x 0, :y 14} {:x 9, :y 10} {:x 0, :y 3}
                      {:x 10, :y 4} {:x 4, :y 11} {:x 6, :y 0} {:x 6, :y 12}
                      {:x 4, :y 1} {:x 0, :y 13} {:x 10, :y 12} {:x 3, :y 4}
                      {:x 3, :y 0} {:x 8, :y 4} {:x 1, :y 10} {:x 2, :y 14}
                      {:x 8, :y 10} {:x 9, :y 0})
        instructions '(["y" 7] ["x" 5])]
    (ct/is (= (d13/coords input) coordinates))
    (ct/is (= (d13/instructions input) instructions))))

(ct/deftest test-transform-points
  (let [a {:x 6, :y 7}]
    (ct/is (= (d13/reflect a ["y" 6]) {:x 6, :y 5}))
    (ct/is (= (d13/reflect a ["y" 4]) {:x 6, :y 1}))
    (ct/is (= (d13/reflect a ["y" 7]) {:x 6, :y 7}))
    (ct/is (= (d13/reflect a ["x" 4]) {:x 2, :y 7}))
    (ct/is (= (d13/reflect a ["x" 5]) {:x 4, :y 7}))
    (ct/is (= (d13/reflect a ["x" 8]) {:x 6, :y 7}))))

(ct/deftest test-parts
  (ct/is (= (d13/part-one "day_13_test.txt") 17))
  (ct/is (= (count (d13/part-two "day_13_test.txt")) 16)))
