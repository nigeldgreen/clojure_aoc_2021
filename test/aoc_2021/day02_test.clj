(ns aoc-2021.day02-test
  (:require [clojure.test :as ct]
            [aoc-2021.day02 :as d2]))

(ct/deftest map-hpos-and-depth-tests
  (ct/is (= [5 0] (d2/map-hpos-and-depth [0 0] ["forward" 5])))
  (ct/is (= [13 5] (d2/map-hpos-and-depth [5 5] ["forward" 8])))
  (ct/is (= [3 3] (d2/map-hpos-and-depth [3 0] ["down" 3])))
  (ct/is (= [7 15] (d2/map-hpos-and-depth [7 25] ["up" 10]))))

(ct/deftest map-hpos-aim-and-depth-tests
  (ct/is (= [5 0 0] (d2/map-hpos-aim-and-depth [0 0 0] ["forward" 5])))
  (ct/is (= [5 0 5] (d2/map-hpos-aim-and-depth [5 0 0] ["down" 5])))
  (ct/is (= [5 0 3] (d2/map-hpos-aim-and-depth [5 0 5] ["up" 2])))
  (ct/is (= [13 29 3] (d2/map-hpos-aim-and-depth [5 5 3] ["forward" 8]))))

(ct/deftest part-one-test
  (ct/is (= 150 (d2/part-one "day_02_test.txt"))))

(ct/deftest part-two-test
  (ct/is (= 900 (d2/part-two "day_02_test.txt"))))
