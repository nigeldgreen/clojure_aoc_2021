(ns aoc-2021.day07-test
  (:require [clojure.test :as ct]
            [aoc-2021.day07 :as d7]
            [aoc-2021.fileio :refer [read-file-to-list]]))

(ct/deftest answers-to-part-one
  (ct/is (= (d7/part-one (read-file-to-list "day_07_test.txt")) 37))
  (ct/is (= (d7/part-one (read-file-to-list "day_07.txt")) 336721))
  )

(ct/deftest answers-to-part-two
  (ct/is (= (d7/part-two (read-file-to-list "day_07_test.txt")) 168))
  (ct/is (= (d7/part-two (read-file-to-list "day_07.txt")) 91638945))
)