(ns aoc-2021.day11-test
  (:require [clojure.test :as ct]
            [aoc-2021.collection-as-list :as cal]
            [aoc-2021.day11 :as d11]))

(ct/deftest can-make-cave-from-input
  (let [input {:length 3 :coll "123234345"}]
    (ct/is (= '({:idx 0, :flashed 0 :val 1, :adj [1 3 4]}
                {:idx 1, :flashed 0 :val 2, :adj [0 2 3 4 5]}
                {:idx 2, :flashed 0 :val 3, :adj [1 4 5]}
                {:idx 3, :flashed 0 :val 2, :adj [0 1 4 6 7]}
                {:idx 4, :flashed 0 :val 3, :adj [0 1 2 3 5 6 7 8]}
                {:idx 5, :flashed 0 :val 4, :adj [1 2 4 7 8]}
                {:idx 6, :flashed 0 :val 3, :adj [3 4 7]}
                {:idx 7, :flashed 0 :val 4, :adj [3 4 5 6 8]}
                {:idx 8, :flashed 0 :val 5, :adj [4 5 7]})(d11/make-cave input)))))

(ct/deftest can-calculate-non-flashing-steps
  (let [cave0 (d11/make-cave {:length 3 :coll "123234345"})
        cave1 (d11/next-step cave0)
        cave2 (d11/next-step cave1)]
    (ct/is (= "234345456" (d11/get-cave-as-string cave1)))
    (ct/is (= "345456567" (d11/get-cave-as-string cave2)))))

(ct/deftest can-calculate-flashing-steps1
  (let [cave0 (d11/make-cave {:length 3 :coll "111191111"})
        cave1 (d11/next-step cave0)
        cave2 (d11/next-step cave1)]
    (ct/is (= "333303333" (d11/get-cave-as-string cave1)))
    (ct/is (= "444414444" (d11/get-cave-as-string cave2)))))

(ct/deftest can-calculate-flashing-steps2
  (let [cave0 (d11/make-cave {:length 5 :coll "1111119991191911999111111"})
        cave1 (d11/next-step cave0)
        cave2 (d11/next-step cave1)]
    (ct/is (= "3454340004500054000434543" (d11/get-cave-as-string cave1)))
    (ct/is (= "4565451115611165111545654" (d11/get-cave-as-string cave2)))))

(ct/deftest can-calculate-flashing-steps3
  (let [cave (d11/make-cave (cal/parse-collection "day_11_test.txt"))
        result (d11/do-n-steps cave 10)]
    (ct/is (= "0481112976003111200900411125040081111406009911130600935112330442361130553225235005322506000032240000"
              (d11/get-cave-as-string result))
    (ct/is (= 204 (:flashes result))))))
