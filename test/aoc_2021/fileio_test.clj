(ns aoc-2021.fileio-test
  (:require [clojure.test :as ct]
            [aoc-2021.fileio :as f]))

(def test-data-strings ["199" "200" "208" "210" "200" "207" "240" "269" "260" "263"])
(def test-data-ints [199 200 208 210 200 207 240 269 260 263])


(ct/deftest get-full-filepath-test
  (ct/testing "Get full filepath, given a filename"
    (ct/is (= "resources/files/test_file.txt" (f/get-full-filepath "test_file.txt")))))

(ct/deftest read-in-file-to-list-test
  (ct/testing "Read in a file with each line as a list item"
    (ct/is (= test-data-strings (f/read-file-to-list "day_01_test.txt")))))

(ct/deftest read-file-as-list-of-ints-test
  (ct/testing "Read in a file winth each line as a list of ints"
    (ct/is (= test-data-ints (f/read-file-as-list-of-ints "day_01_test.txt")))))

