(ns aoc-2021.day03-test
  (:require [clojure.test :as ct]
            [aoc-2021.day03 :as d3]
            [aoc-2021.fileio :refer [read-file-to-list]]))

(def input (read-file-to-list "day_03_test.txt"))

(ct/deftest nth-column-tests
  (ct/is (= '(0 1 1 1 1 0 0 1 1 1 0 0) (d3/nth-column 0 input)))
  (ct/is (= '(0 1 0 0 0 1 0 1 0 1 0 1) (d3/nth-column 1 input)))
  (ct/is (= '(1 1 1 1 1 1 1 1 0 0 0 0) (d3/nth-column 2 input)))
  (ct/is (= '(0 1 1 1 0 1 1 0 0 0 1 1) (d3/nth-column 3 input)))
  (ct/is (= '(0 0 0 1 1 1 1 0 0 1 0 0) (d3/nth-column 4 input))))

(ct/deftest gamma-rate-tests
  (ct/is (= 1 (d3/most-pop (d3/nth-column 0 input))))
  (ct/is (= 0 (d3/most-pop (d3/nth-column 1 input))))
  (ct/is (= 1 (d3/most-pop (d3/nth-column 2 input))))
  (ct/is (= 1 (d3/most-pop (d3/nth-column 3 input))))
  (ct/is (= 0 (d3/most-pop (d3/nth-column 4 input)))))

(ct/deftest epsilon-rate-tests
  (ct/is (= 0 (d3/least-pop (d3/nth-column 0 input))))
  (ct/is (= 1 (d3/least-pop (d3/nth-column 1 input))))
  (ct/is (= 0 (d3/least-pop (d3/nth-column 2 input))))
  (ct/is (= 0 (d3/least-pop (d3/nth-column 3 input))))
  (ct/is (= 1 (d3/least-pop (d3/nth-column 4 input)))))

(ct/deftest part-one-test
  (ct/is (= 198 (d3/part-one input))))

(ct/deftest oxygen-rate-tests
  (ct/is (= "10111" (d3/reduce-values input 0 d3/most-pop)))
  (ct/is (= "01010" (d3/reduce-values input 0 d3/least-pop))))

(ct/deftest part-two-test
  (ct/is (= 230 (d3/part-two input))))
