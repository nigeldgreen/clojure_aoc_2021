(ns aoc-2021.day05-test
  (:require [clojure.test :as ct]
            [aoc-2021.day05 :as d5]
            [aoc-2021.fileio :refer [read-file-to-list]]))

(def test-input (read-file-to-list "day_05_test.txt"))
(def lines '({:p1x 0 :p1y 9 :p2x 5 :p2y 9}
            {:p1x 8 :p1y 0 :p2x 0 :p2y 8}
            {:p1x 9 :p1y 4 :p2x 3 :p2y 4}
            {:p1x 2 :p1y 2 :p2x 2 :p2y 1}
            {:p1x 7 :p1y 0 :p2x 7 :p2y 4}
            {:p1x 6 :p1y 4 :p2x 2 :p2y 0}
            {:p1x 0 :p1y 9 :p2x 2 :p2y 9}
            {:p1x 3 :p1y 4 :p2x 1 :p2y 4}
            {:p1x 0 :p1y 0 :p2x 8 :p2y 8}
            {:p1x 5 :p1y 5 :p2x 8 :p2y 2}))

(ct/deftest get-correct-map-from-input
  (ct/is (= lines (d5/get-lines test-input)))
)

(ct/deftest can-get-intermediate-values-from-start-and-end-points
  (ct/is (= [0 1 2 3 4 5] (d5/get-values 0 5)))
  (ct/is (= [8 7 6 5 4] (d5/get-values 8 4)))
  (ct/is (= 3 (d5/get-values 3 3)))
)

(ct/deftest can-find-all-points-for-a-line
  (ct/is (= '([0 9] [1 9] [2 9] [3 9] [4 9] [5 9])
            (d5/get-points {:p1x 0 :p1y 9 :p2x 5 :p2y 9})))
  (ct/is (= '([2 2] [2 1])
            (d5/get-points {:p1x 2 :p1y 2 :p2x 2 :p2y 1})))
)

(ct/deftest can-find-correct-part-answers-test-data
  (ct/is (= 5
            (d5/part-one test-input)))
  (ct/is (= 12
            (d5/part-two test-input)))
)

(ct/deftest can-find-correct-part-answers-real-data
  (ct/is (= 5197
            (d5/part-one (read-file-to-list "day_05.txt"))))
  (ct/is (= 18605
            (d5/part-two (read-file-to-list "day_05.txt"))))
)
