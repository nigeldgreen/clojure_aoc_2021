(ns aoc-2021.day14-test
  (:require [clojure.test :as ct]
            [aoc-2021.fileio :as io]
            [aoc-2021.day14 :as d14]
            [clojure.string :as str]))

(ct/deftest test-can-see-correct-map
  (let [map0 {:B  1
              :C  1
              :H  0
              :N  2
              :BB {:count 0 :adds :N :spawns [:BN :NB]}
              :BC {:count 0 :adds :B :spawns [:BB :BC]}
              :BH {:count 0 :adds :H :spawns [:BH :HH]}
              :BN {:count 0 :adds :B :spawns [:BB :BN]}
              :CB {:count 1 :adds :H :spawns [:CH :HB]}
              :CC {:count 0 :adds :N :spawns [:CN :NC]}
              :CH {:count 0 :adds :B :spawns [:CB :BH]}
              :CN {:count 0 :adds :C :spawns [:CC :CN]}
              :HB {:count 0 :adds :C :spawns [:HC :CB]}
              :HC {:count 0 :adds :B :spawns [:HB :BC]}
              :HH {:count 0 :adds :N :spawns [:HN :NH]}
              :HN {:count 0 :adds :C :spawns [:HC :CN]}
              :NB {:count 0 :adds :B :spawns [:NB :BB]}
              :NC {:count 1 :adds :B :spawns [:NB :BC]}
              :NH {:count 0 :adds :C :spawns [:NC :CH]}
              :NN {:count 1 :adds :C :spawns [:NC :CN]}}
        map1 {:B  2
              :C  2
              :H  1
              :N  2
              :BB {:count 0 :adds :N :spawns [:BN :NB]}
              :BC {:count 1 :adds :B :spawns [:BB :BC]}
              :BH {:count 0 :adds :H :spawns [:BH :HH]}
              :BN {:count 0 :adds :B :spawns [:BB :BN]}
              :CB {:count 0 :adds :H :spawns [:CH :HB]}
              :CC {:count 0 :adds :N :spawns [:CN :NC]}
              :CH {:count 1 :adds :B :spawns [:CB :BH]}
              :CN {:count 1 :adds :C :spawns [:CC :CN]}
              :HB {:count 1 :adds :C :spawns [:HC :CB]}
              :HC {:count 0 :adds :B :spawns [:HB :BC]}
              :HH {:count 0 :adds :N :spawns [:HN :NH]}
              :HN {:count 0 :adds :C :spawns [:HC :CN]}
              :NB {:count 1 :adds :B :spawns [:NB :BB]}
              :NC {:count 1 :adds :B :spawns [:NB :BC]}
              :NH {:count 0 :adds :C :spawns [:NC :CH]}
              :NN {:count 0 :adds :C :spawns [:NC :CN]}}
        map2 {:B  6
              :C  4
              :H  1
              :N  2
              :BB {:count 2 :adds :N :spawns [:BN :NB]}
              :BC {:count 2 :adds :B :spawns [:BB :BC]}
              :BH {:count 1 :adds :H :spawns [:BH :HH]}
              :BN {:count 0 :adds :B :spawns [:BB :BN]}
              :CB {:count 2 :adds :H :spawns [:CH :HB]}
              :CC {:count 1 :adds :N :spawns [:CN :NC]}
              :CH {:count 0 :adds :B :spawns [:CB :BH]}
              :CN {:count 1 :adds :C :spawns [:CC :CN]}
              :HB {:count 0 :adds :C :spawns [:HC :CB]}
              :HC {:count 1 :adds :B :spawns [:HB :BC]}
              :HH {:count 0 :adds :N :spawns [:HN :NH]}
              :HN {:count 0 :adds :C :spawns [:HC :CN]}
              :NB {:count 2 :adds :B :spawns [:NB :BB]}
              :NC {:count 0 :adds :B :spawns [:NB :BC]}
              :NH {:count 0 :adds :C :spawns [:NC :CH]}
              :NN {:count 0 :adds :C :spawns [:NC :CN]}}]
    (ct/is (= (d14/parse-input "day_14_test.txt") map0))
    (ct/is (= (d14/next-step map0) map1))
    (ct/is (= (d14/next-step map1) map2))
    (ct/is (= (d14/nth-step map0 1) map1))
    (ct/is (= (d14/nth-step map0 2) map2))))

(ct/deftest test-answers
  (ct/is (= (d14/get-answer "day_14_test.txt" 1) 1))
  (ct/is (= (d14/get-answer "day_14_test.txt" 2) 5))
  (ct/is (= (d14/get-answer "day_14_test.txt" 10) 1588))
  (ct/is (= (d14/get-answer "day_14_test.txt" 40) 2188189693529)))

(ct/deftest test-parts
  (ct/is (= (d14/get-answer "day_14.txt" 10) 2797))
  (ct/is (= (d14/get-answer "day_14.txt" 40) 2926813379532))
)
