(ns aoc-2021.day09-test
  (:require [clojure.test :as ct]
            [aoc-2021.day09 :as d9]
            [aoc-2021.fileio :refer [read-file-to-list]]))

(ct/deftest can-get-points-from-input-list
  (ct/is (= (d9/build-points [8 7 6 5 4 3 2 1 9 3 8 4] 4)
            [{:index 0 :value 8 :adjacents [[1 7] [4 4]]},
             {:index 1 :value 7 :adjacents [[0 8] [2 6] [5 3]]},
             {:index 2 :value 6 :adjacents [[1 7] [3 5] [6 2]]},
             {:index 3 :value 5 :adjacents [[2 6] [7 1]]},
             {:index 4 :value 4 :adjacents [[0 8] [5 3] [8 9]]},
             {:index 5 :value 3 :adjacents [[1 7] [4 4] [6 2] [9 3]]},
             {:index 6 :value 2 :adjacents [[2 6] [5 3] [7 1] [10 8]]},
             {:index 7 :value 1 :adjacents [[3 5] [6 2] [11 4]]},
             {:index 8 :value 9 :adjacents [[4 4] [9 3]]},
             {:index 9 :value 3 :adjacents [[5 3] [8 9] [10 8]]},
             {:index 10 :value 8 :adjacents [[6 2] [9 3] [11 4]]},
             {:index 11 :value 4 :adjacents [[7 1] [10 8]]}])))

(ct/deftest can-get-sum-of-adjacents-for-point
  (ct/is (= (d9/min-of-adjacents {:index 0 :value 8 :adjacents [[1 7] [4 4]]})
            4))
  (ct/is (= (d9/min-of-adjacents {:index 4 :value 4 :adjacents [[0 8] [5 3] [8 9]]})
            3))
  (ct/is (= (d9/min-of-adjacents {:index 6 :value 2 :adjacents [[2 6] [5 3] [7 1] [10 8]]})
            1)))

(def raw (read-file-to-list "day_09_test.txt"))
(def length (count (first raw)))
(def input (d9/parse-input raw))
(def p-map (d9/build-points input length))

(ct/deftest can-get-next-valid-adjacents
  (ct/is (= (d9/next-in-basin [{:index 0 :value 8 :adjacents [[1 7] [4 4]]}] p-map)
            nil))
  (ct/is (= (d9/next-in-basin [{:index 1, :value 1, :adjacents [[0 2] [2 9] [11 9]]}] p-map)
            [{:index 0, :value 2, :adjacents [[1 1] [10 3]]}]))
  (ct/is (= (d9/next-in-basin [{:index 9, :value 0, :adjacents [[8 1] [19 1]]}] p-map)
            [{:index 19, :value 1, :adjacents [[9 0] [18 2] [29 2]]}
             {:index 8, :value 1, :adjacents [[7 2] [9 0] [18 2]]}]))
  (ct/is (= (d9/next-in-basin [{:index 8, :value 1, :adjacents [[7 2] [9 0] [18 2]]}
                               {:index 19, :value 1, :adjacents [[9 0] [18 2] [29 2]]}] p-map)
            [{:index 7, :value 2, :adjacents [[6 3] [8 1] [17 9]]}
             {:index 18, :value 2, :adjacents [[8 1] [17 9] [19 1] [28 9]]}
             {:index 29, :value 2, :adjacents [[19 1] [28 9] [39 9]]}])))

(ct/deftest can-build-basin-of-correct-size
  (ct/is (= (count (d9/build-basin {:index 1, :value 1, :adjacents [[0 2] [2 9] [11 9]]} p-map))
            3))
  (ct/is (= (count (d9/build-basin {:index 9, :value 0, :adjacents [[8 1] [19 1]]} p-map))
            9))
  (ct/is (= (count (d9/build-basin {:index 22, :value 5, :adjacents [[12 8] [21 8] [23 6] [32 6]]} p-map))
            14))
  (ct/is (= (count (d9/build-basin {:index 46, :value 5, :adjacents [[36 6] [45 6] [47 6]]} p-map))
            9)))

(ct/deftest answers-to-part-one
  (ct/is (= (d9/part-one (read-file-to-list "day_09_test.txt")) 15))
  (ct/is (= (d9/part-one (read-file-to-list "day_09.txt")) 417)))

(ct/deftest answers-to-part-two
  (ct/is (= (d9/part-two (read-file-to-list "day_09_test.txt")) 1134))
  (ct/is (= (d9/part-two (read-file-to-list "day_09.txt")) 1148965)))