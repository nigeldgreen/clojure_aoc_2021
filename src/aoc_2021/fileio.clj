(ns aoc-2021.fileio
  (:gen-class)
  (:require
   [clojure.string :as str]))

(defn get-full-filepath
  "Return a string with full filepath"
  [filename]
  (str "resources/files/" filename))

(defn read-file-to-list
  "Read an input file and split each line to a vector"
  [file]
  (str/split-lines (slurp (get-full-filepath file))))

(defn read-file-as-list-of-ints
  "Take a split file and cast each line to an int value"
  [file]
  (map bigint (read-file-to-list file)))
