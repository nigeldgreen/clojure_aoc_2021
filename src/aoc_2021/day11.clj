(ns aoc-2021.day11
  (:gen-class)
  (:require [aoc-2021.collection-as-list :as cal]
            [clojure.string :refer [join split]]))

(defn make-cave [input]
  {:flashes 0
   :cave (vec (map-indexed (fn [idx item]
                          {:idx idx, :val (Integer/parseInt (str item)), :flashed 0, :adj (cal/get-all-adjacents input idx)})
                        (:coll input)))})

(defn get-cave-as-string [cave]
  (->> (:cave cave)
       (map #(str (:val %)))
       join))

(defn full-flash? [cave]
  (every? #(= "0" %) (split (get-cave-as-string cave) #"")))

(defn init-octos [cave]
  (->> cave
       (mapv #(assoc % :flashed 0 :changing false))))

(defn transform-octo [octo]
  (let [v (:val octo)
        flashed (:flashed octo)
        changing (= v 9)]
    (assoc octo :val (if (or (= 1 flashed) changing) 0 (inc v))
                :flashed (if (or (= 1 flashed) changing) 1 0)
                :changing changing)))

(defn next-step [c]
  (loop [cave (init-octos (:cave c))
         idxs (mapv #(:idx %) (:cave c))
         flashes (:flashes c)]
    (if (= [] idxs)
      {:cave cave :flashes flashes}
      (let [octo (transform-octo (nth cave (first idxs)))]
        (recur (assoc cave (first idxs) octo)
               (if (:changing octo)
                 (apply conj (rest idxs) (:adj octo))
                 (rest idxs))
               (if (:changing octo) (inc flashes) flashes))))))

(defn do-n-flashes [c n]
  (loop [cave c
         times n]
    (if (= 0 times) cave (recur (next-step cave) (dec times)))))

; Answer to part one - 1647
(let [cave (make-cave (cal/parse-collection "day_11.txt"))]
  (:flashes (do-n-flashes cave 100)))

; Answer to part two - 348
(loop [cave (make-cave (cal/parse-collection "day_11.txt"))
       step 0]
  (if (full-flash? cave) step (recur (next-step cave) (inc step))))
