(ns aoc-2021.day13
  (:gen-class)
  (:require [aoc-2021.fileio :as io]
            [clojure.string :as str]))

(defn coords [input]
  (->> input
       (filter (fn [x] (and (not= "" x) (not= \f (first x)))))
       (map (fn [x] (str/split x #",")))
       (map (fn [x] {:x (Integer/parseInt (str (first x))) :y (Integer/parseInt (str (last x)))}))
       vec))

(defn instructions [input]
  (->> input
       (filter (fn [x] (= \f (first x))))
       (map #(str/split % #" "))
       (map #(str/split (last %) #"="))
       (map (fn [x] [(first x) (Integer/parseInt (str (last x)))]))))

(defn reflect [point line]
  (let [axis (keyword (first line))
        value (peek line)]
    (if (<= (axis point) value)
      point
      (assoc point axis (- value (- (axis point) value))))))

(defn map-points [points line]
  (->> points (map #(reflect % line)) set vec))

(defn idx-from-coord [c xmax]
  (+ (:x c) (* xmax (:y c))))

(defn get-letters [input]
  (let [xmax (->> input (map #(:x %)) (apply max))
        ymax (->> input (map #(:y %)) (apply max))]
    (loop [result (->> (take (* (inc xmax) (inc ymax)) (repeat (str " ")))
                       (apply concat)
                       (map #(str %))
                       vec)
           idx (->> input (map #(idx-from-coord % xmax)))]
      (if (= 0 (count idx))
        (->> result (partition xmax) (map #(str/join %)))
        (recur (assoc result (first idx) "|") (rest idx))))))

(defn part-one [file]
  (let [i (io/read-file-to-list file)]
    (count (map-points (coords i) (first (instructions i))))))

(defn part-two [file]
  (let [input (io/read-file-to-list file)]
    (loop [c (coords input)
           i (instructions input)]
      (if (= 0 (count i))
        (get-letters c)
        (recur (map-points c (first i)) (rest i))))))

(comment
  (part-one "day_13.txt")     ; 610
  (part-two "day_13.txt")     ; PZFJHRFZ
)