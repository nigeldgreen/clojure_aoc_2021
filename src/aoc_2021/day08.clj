(ns aoc-2021.day08
  (:gen-class)
  (:require [clojure.string :refer [split join]]
            [clojure.set :refer [intersection difference]]))

(defn parse-input [input]
  (->> input
       (map #(split % #" \| "))
       (map (fn [[k v]]
              {:inputs (->> (split k #" ")
                           (map #(join (sort %))))
               :outputs (->> (split v #" ")
                            (map #(join (sort %))))}))))

(defn get-letter-by-segment-number
  "Given a list of inputs, return the number with n segments"
  [inputs n]
  (split (->> inputs
              (filter #(= n (count %)))
              first) #""))

(defn num-contains-num?
  "Given two letter representations, work out if the segments in n1 are contained in n2"
  [n1 n2]
  (= (count n1) (count (intersection (set n1) (set n2)))))

(defn letter-diff
  "Given two letter representations, work out the letter in n1 not in n2"
  [n1 n2]
  (difference (set n1) (set n2)))

(defn get-numbers
  "Use the inputs from a row to work out which letter maps to which segment, then use the result to
  pass back a map of all the sorted letter maps for each number that can be made."
  [inputs]
  (let [fives (map #(split % #"") (filter #(= 5 (count %)) inputs))
        eight (get-letter-by-segment-number inputs 7)
        one   (get-letter-by-segment-number inputs 2)
        seven (get-letter-by-segment-number inputs 3)
        s0    (->> (letter-diff seven one) first)
        four  (get-letter-by-segment-number inputs 4)
        three (->> (filter #(num-contains-num? one %) fives)
                   first)
        s1    (->> (letter-diff four three) first)
        s3    (->> (letter-diff four one) (filter #(not= s1 %)) first)
        s6    (->> (letter-diff three four) (filter #(not= s0 %)) first)
        five  (->> (filter #(num-contains-num? [s1] %) fives)
                   first)
        s2    (->> (letter-diff one five) first)
        s5    (->> (intersection (set one) (set five))
                   first
                   (apply str))
        two   (->> (filter #(and (not= three %) (not= five %)) fives)
                   first)
        s4    (->> (letter-diff two three) first)
        zero  (->> (str s0 s1 s2 s4 s5 s6) sort join)
        six   (->> (str s0 s1 s3 s4 s5 s6) sort join)
        nine  (->> (str s0 s1 s2 s3 s5 s6) sort join)]
    {(apply str zero)  0
     (apply str one) 1
     (apply str two) 2
     (apply str three) 3
     (apply str four)  4
     (apply str five)  5
     (apply str six)   6
     (apply str seven) 7
     (apply str eight) 8
     (apply str nine)  9}))

(defn get-output-sum
  "Get a map of the decoded inputs and use this to get a value for each output string"
  [row]
  (let [nums (get-numbers (:inputs row))
        outputs (:outputs row)]
    (map #(nums %) outputs)))

(defn part-one [input]
  (->> (parse-input input)
       (map :outputs)
       (apply concat)
       (map #(count %))
       frequencies
       (filter #(or (= 2 (key %)) (= 3 (key %)) (= 4 (key %)) (= 7 (key %))))
       vals
       (reduce +)))

(defn part-two [input]
  (->> (parse-input input)
       (map #(get-output-sum %))
       (map #(Integer/parseInt (join %)))
       (reduce +)))
