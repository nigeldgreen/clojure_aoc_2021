(ns aoc-2021.day04
  (:gen-class)
  (:require [aoc-2021.fileio :refer [read-file-to-list]]
            [clojure.string :refer [split join trim]]))

(def input (read-file-to-list "day_04.txt"))

(defn get-call-nums [input]
  (->> (split (first input) #",")
       (map #(Integer/parseInt %))))

(defn get-cards [input]
  (->> (split (->> input rest (join " ") trim) #" +")
       (map #(Integer/parseInt %))
       (partition 25)))

(defn get-lines [card]
  (partition 5 5 card))

(defn get-cols [card]
  (map #(flatten (partition 1 5 (drop % card))) [0 1 2 3 4]))

(defn winner? [card]
  (->> (get-lines card)
       (concat (get-cols card))
       (some #(= '(\x \x \x \x \x) %))))

(defn mark-card [card num] (map #(if (= % num) \x %) card))

(defn make-call [cards num] (map #(mark-card % num) cards))

(defn check-cards [cards] (first (filter #(winner? %) cards)))

(defn winning-value [vals]
  (* (peek vals) (reduce + (filter #(not= \x %) (first vals)))))

(defn play-part-one [nums cards]
  (let [num (first nums)
        new-cards (make-call cards num)
        winner (check-cards new-cards)]
    (if (nil? winner)
      (play-part-one (rest nums) new-cards)
      [winner num])))

(defn play-part-two [nums cards]
  (let [num (first nums)
        new-cards (make-call cards num)]
    (if (and (= 1 (count new-cards)) (winner? (first new-cards)))
      [(first new-cards) num]
      (play-part-two (rest nums) (filter #(not (winner? %)) new-cards)))))