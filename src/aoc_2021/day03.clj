(ns aoc-2021.day03
  (:gen-class)
  (:require [aoc-2021.fileio :refer [read-file-to-list]]))

(defn nth-column [n input]
  (map #(Character/digit (get % n) 10) input))

(defn most-pop [col]
  (if (> (get (frequencies col) 0)
         (get (frequencies col) 1)) 0 1))

(defn least-pop [col] (if (= 0 (most-pop col)) 1 0))

(defn get-rate
  "Calculate the gamma or epsilon rate across all columns of the input"
  [count fn input]
  (->> (range count)                 ;; how many columns are we dealing with?
       (map #(nth-column % input))   ;; get all the values in the nth column
       (map fn)                      ;; turn each set into a value for the gamma or epsilon bit
       (apply str)))                 ;; and produce a string of all the values

(defn part-one [s]
  (let [chars (count (first s))]
    (* (Integer/parseInt (get-rate chars most-pop s) 2)
       (Integer/parseInt (get-rate chars least-pop s) 2))))

(part-one (read-file-to-list "day_03.txt"))

(defn reduce-values
  ([input fn] (reduce-values input 0 fn))
  ([input idx fn]
   (let [match (str (fn (nth-column idx input)))
         filtered (filter #(= (str (get % idx)) match) input)]
     (if (= 1 (count filtered))
       (first filtered)
       (recur filtered (inc idx) fn)))))

(defn part-two [input]
  (*
   (Integer/parseInt (reduce-values input most-pop) 2)
   (Integer/parseInt (reduce-values input least-pop) 2)))

(part-two (read-file-to-list "day_03.txt")) ;; 5852595
