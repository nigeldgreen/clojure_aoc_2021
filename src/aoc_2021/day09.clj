(ns aoc-2021.day09
  (:gen-class)
  (:require [clojure.string :refer [split join]]))

(defn parse-input [input]
  (->> (split (join input) #"") (mapv #(Integer/parseInt %))))

(defn get-adjacents
  "Get a vector of index/value pairs for a given index in a given collection. "
  [idx c l]
  (let [top (- idx l)
        left (if (= 0 (mod idx l)) -1 (- idx 1))
        right (if (= (- l 1) (mod idx l)) -1 (+ idx 1))
        bottom (+ idx l)]
    (->> [top left right bottom]
         (mapv (fn [idx] [idx (get c idx)]))
         (filterv #(not (nil? (peek %)))))))

(defn build-points
  "Given a collection and it's length, build a map of all the points with index, value and adjacent points"
  [c l]
  (loop [idx 0, points [], coll c]
    (if (= 0 (count coll))
      points
      (recur (inc idx)
             (conj points {:index     idx
                           :value     (first coll)
                           :adjacents (get-adjacents idx c l)})
             (rest coll)))))

(defn min-of-adjacents [p]
  (->> (:adjacents p) (map #(peek %)) (apply min)))

(defn low-point? [p]
  (< (:value p) (min-of-adjacents p)))

(defn next-in-basin
  "Given a vector of points, returns a new vector of all the adjacent points that make a valid part of the basin."
  [points p-map]
  (let [next (->> points
                  (mapv (fn [p] (filter #(and (< (peek %) 9) (> (peek %) (:value p))) (:adjacents p))))
                  (apply concat)
                  set
                  (mapv #(get p-map (first %))))]
    (if (= 0 (count next)) nil next)))

(defn build-basin
  "Given a starting point, build and return a vector containing all the points in it's basin."
  [p p-map]
  (loop [new-points [p], in-basin []]
    (if (nil? new-points)
      (set in-basin)
      (recur (next-in-basin new-points p-map)
             (apply conj in-basin new-points)))))

(defn part-one [f]
  (let [i f, l (count (get i 0)), c (parse-input i)]
    (->> (build-points c l)
         (filter low-point?)
         (map #(inc (:value %)))
         (apply +))))

(defn part-two [f]
  (let [i f, l (count (get i 0)), c (parse-input i), p-map (build-points c l)]
    (reduce * (take 3 (->> p-map
                           (filter low-point?)
                           (map #(count (build-basin % p-map)))
                           sort
                           reverse)))))
