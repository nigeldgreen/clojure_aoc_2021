(ns aoc-2021.day06
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-fish [input]
  (->> input
       (map #(str/split % #","))
       (first)
       (map #(Integer/parseInt %))
       frequencies
       (merge-with + {0 0, 1 0, 2 0, 3 0, 4 0, 5 0, 6 0, 7 0, 8 0})
       (into (sorted-map-by <))))

(defn after-n-days [n f]
  (if (= 0 n) f
    (recur (dec n) {0 (get f 1) 1 (get f 2) 2 (get f 3) 3 (get f 4) 4 (get f 5) 5 (get f 6) 6 (+ (get f 0) (get f 7)) 7 (get f 8) 8 (get f 0)})))

(defn part-one [input] (reduce + (vals (after-n-days 80 (get-fish input)))))

(defn part-two [input] (reduce + (vals (after-n-days 256 (get-fish input)))))
