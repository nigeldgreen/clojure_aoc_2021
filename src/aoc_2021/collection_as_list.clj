(ns aoc-2021.collection-as-list
  (:gen-class)
  (:require [clojure.string :as str]
            [aoc-2021.fileio :refer [read-file-to-list]]))

;; Some helper functions for basic checks
(defn in-first-column? [idx coll] (= 0 (mod idx (:length coll))))
(defn in-last-column? [idx coll] (= 0 (mod (inc idx) (:length coll))))
(defn in-first-row? [idx coll] (< idx (:length coll)))
(defn in-last-row? [idx coll] (<= (- (count (:coll coll)) (:length coll)) idx))

(defn parse-collection
  "Build a map with :coll for the collection as a single string and :length for the row length"
  [file]
  (let [raw (read-file-to-list file)
        l (count (first raw))]
    {:length l :coll (str/join #"" raw)}))

(defn get-verticals
  "Return the index in the collection for the top and bottom adjacents for a given index"
  [coll idx]
  (let [l (:length coll)
        top (if (in-first-row? idx coll) -1 (- idx l))
        bottom (if (in-last-row? idx coll) -1 (+ idx l))]
  [top bottom]))

(defn get-horizontals
  "Return the index in the collection for the left and right adjacents for a given index"
  [coll idx]
  (let [left (if (in-first-column? idx coll) -1 (- idx 1))
        right (if (in-last-column? idx coll) -1 (+ idx 1))]
  [left right]))

(defn get-diagonals
  "Return the index in the collection for the diagonal adjacents for a given index"
  [coll idx]
  (let [l (:length coll)
        top-left (if (or (in-first-row? idx coll) (in-first-column? idx coll)) -1 (- idx (inc l)))
        top-right (if (or (in-first-row? idx coll) (in-last-column? idx coll)) -1 (- idx (dec l)))
        bottom-left (if (or (in-last-row? idx coll) (in-first-column? idx coll)) -1 (+ idx (dec l)))
        bottom-right (if (or (in-last-row? idx coll) (in-last-column? idx coll)) -1 (+ idx (inc l)))]
    [top-left top-right bottom-left bottom-right]))

(defn get-strict-adjacents
  "Get a vector of index/value pairs for a given index in a given collection"
  [coll idx]
  (let [[top bottom] (get-verticals coll idx)
        [left right] (get-horizontals coll idx)]
    (->> [top left right bottom]
         (filterv #(>= % 0)))))

(defn get-all-adjacents
  "Get a vector of index/value pairs for a given index in a given collection"
  [coll idx]
  (let [[top bottom] (get-verticals coll idx)
        [left right] (get-horizontals coll idx)
        [topleft topright bottomleft bottomright] (get-diagonals coll idx)]
    (->> [topleft top topright left right bottomleft bottom bottomright]
         (filterv #(>= % 0)))))
