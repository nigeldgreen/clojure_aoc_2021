(ns aoc-2021.day14
  (:require [aoc-2021.fileio :as io]
            [clojure.string :as str]))

(defn get-spawn [x y]
  (let [[a b] (str/split x #"")]
    [(keyword (str/join [a y])) (keyword (str/join [y b]))]))

(defn initialise-pairs [input]
  (->> input
       (drop 2)
       (map #(str/split % #" -> "))
       (map (fn [[x y]] [(keyword x) {:count 0 :adds (keyword y) :spawns (get-spawn x y)}]))
       (into {})))

(defn count-polymer [p]
  (->> (str/split p #"")
       frequencies
       (map (fn [x] [(keyword (first x)) (last x)]))
       (into {})))

(defn get-adds [input]
  (->> input
       (drop 2)
       (map #(str/split % #" -> "))
       (map #(keyword ( last %)))
       set vec))

(defn build-map [input polymer]
  (let [init-map (initialise-pairs input)
        counts (count-polymer polymer)
        adds (get-adds input)]
    (reduce #(assoc %1 %2 (or (%2 counts) 0)) init-map adds)))

(defn increase-count-for-pair [pair input]
  (let [p (keyword pair)]
    (assoc-in input [(keyword p) :count] (inc (:count (p input))))))

(defn parse-input [file]
  (let [input (io/read-file-to-list file)
        polymer (first input)
        pairs (->> polymer (partition 2 1) (map #(apply str %)))]
    (reduce #(increase-count-for-pair %2 %1)
            (build-map input polymer)
            pairs)))

(defn pair-keys [m] (->> m keys (filter #(< 2 (count (str %))))))

(defn zero-count [input] (reduce #(assoc-in %1 [%2 :count] 0) input (pair-keys input)))

(defn update-spawn [m p] (reduce #(assoc-in %1 [%2 :count] (+ (:count p) (:count (%2 %1)))) m (:spawns p)))

(defn update-letter-count [m p] (assoc m (:adds p) (+ (:count p) ((:adds p) m))))

(defn update-all [m p] (update-spawn (update-letter-count m p) p))

(defn next-step [m]
  (let [source m
        dest (zero-count source)]
    (reduce #(update-all %1 (%2 source)) dest (pair-keys source))))

(defn nth-step [m n]
    (if (= 0 n) m (recur (next-step m) (dec n))))

(defn get-answer [f n]
  (let [final (nth-step (parse-input f) n)
        values (->> (io/read-file-to-list f) get-adds (map #(% final)))
        highest (apply max values)
        lowest (apply min values)]
    (- highest lowest)))
