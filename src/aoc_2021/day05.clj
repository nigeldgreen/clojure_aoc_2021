(ns aoc-2021.day05
  (:gen-class)
  (:require [clojure.string :as str :refer [split join]]))

(defn get-lines [input]
  (->> input
        (map #(split % #" -> "))
        (map #(join " " %))
        (map #(str/replace % "," " "))
        (map #(split % #" "))
        (map (fn [v] (map #(Integer/parseInt %) v)))
        (map #(zipmap [:p1x :p1y :p2x :p2y] %))))

(defn get-values [p1 p2]
  (if (= p1 p2) p1
    (if (< p1 p2)
      (conj (vec (range p1 p2)) p2)
      (conj (vec (range p1 p2 -1)) p2))))

(defn get-points [line]
  (let [x (get-values (:p1x line) (:p2x line))
        y (get-values (:p1y line) (:p2y line))]
    (map vector
         (if (number? x) (repeat x) x)
         (if (number? y) (repeat y) y))))

(defn get-answer [input]
  (->> input
       (map #(get-points %))
       (apply concat)
       frequencies
       vals
       (filter #(<= 2 %))
       count))

(defn part-one [input]
  (->> (get-lines input)
       (filter #(or (= (:p1y %) (:p2y %)) (= (:p1x %) (:p2x %))))
       (get-answer)))

(defn part-two [input]
  (->> (get-lines input)
       (get-answer)))
