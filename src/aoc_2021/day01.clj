(ns aoc-2021.day01
  (:gen-class)
  (:require [aoc-2021.fileio :refer [read-file-as-list-of-ints]]))

(defn deeper?
  [pair]
  (< (first pair) (last pair)))

(defn part-one
  [file]
  (let [input (read-file-as-list-of-ints file)]
    (count
     (filter deeper? (partition 2 1 input)))))

(part-one "day_01.txt")

(defn part-two
  [file]
  (let [input (read-file-as-list-of-ints file)]
    (count
      (filter deeper? (partition 2 1 (map #(reduce + %) (partition 3 1 input)))))))

(part-two "day_01.txt")
