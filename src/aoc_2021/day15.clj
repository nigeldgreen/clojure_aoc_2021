(ns aoc-2021.day15
  ( :require [aoc-2021.fileio :as io]
  [aoc-2021.collection-as-list :as cal]
  [clojure.string :as str]))

(defn in-path? [next path] (not ( nil? (some #{next} path))))
(defn is-end? [path coll] (= (dec (:length coll)) (peek path)))

(def grid (cal/parse-collection "day_15_test_9.txt"))
(def themap (->> (:coll grid)
     (map-indexed (fn [idx _] {:idx idx :adj (cal/get-strict-adjacents grid idx)}))))

(some #{4} [0 1 2])
