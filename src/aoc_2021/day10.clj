(ns aoc-2021.day10
  (:gen-class)
  (:require [clojure.string :refer [split]]))

(defn process-line
  "Take a line and process character by character to see if it is valid or has a syntax error.
  If the line is valid, return the stack to calculate the value of completing the list (part two).
  If it's not, return the breaking character to calculate the value of the errors (part one)."
  [line]
  (loop [l line, stack []]
    (let [matches {")" "(", "]" "[", "}" "{", ">" "<"}
          next (first l)]
      (if (= 0 (count l))
        stack
        ;; If the next char is an opener, add it to the stack and loop
        (if (nil? (get matches next))
          (recur (rest l) (conj stack next))
          ;; If not, check for a match and either loop with a new stack or return the character as a syntax error
          (if (= (get matches next) (peek stack))
            (recur (rest l) (pop stack))
            next))))))

(defn score-stack
  "Calculate a score for a returned stack. Use pop here so that we process the stack in the correct order."
  [stack]
  (let [points {"(" 1, "[" 2, "{" 3, "<" 4}]
    (loop [score 0, s stack]
      (if (= 0 (count s))
        score
        (recur (+ (get points (peek s)) (* 5 score))
               (pop s))))))

(defn part-one [input]
  (let [points {")" 3, "]" 57, "}" 1197, ">" 25137}]
    (->> input
         (map #(split % #""))
         (map #(process-line %))
         (filter #(not (vector? %)))
         (map #(get points %))
         (reduce +))))

(defn part-two [input]
  (let [results (->> input
                     (map #(split % #""))
                     (map #(process-line %))
                     (filterv #(vector? %))
                     (mapv #(score-stack %))
                     sort
                     vec)
        middle (int (/ (count results) 2.0))]
    (get results middle)))