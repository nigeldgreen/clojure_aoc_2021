(ns aoc-2021.day07
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-crabs [input]
  (->> input
       (map #(str/split % #","))
       (first)
       (map #(Integer/parseInt %))
       frequencies))

(defn get-distance [target crab]
  (apply - (sort > [(key crab) target])))

(defn distance-to-target [target crabs fuel-fn]
  (->> crabs
       (map #(* (fuel-fn (get-distance target %)) (val %)))
       (reduce +)))

(defn calculate-moves [crabs positions fuel-fn]
  (->> positions
       (map #(distance-to-target % crabs fuel-fn))
       (apply min)))

(defn part-one [input]
  (let [crabs (get-crabs input)]
    (calculate-moves crabs
                     (range (+ 1 (apply max (keys crabs))))
                     identity)))

(defn part-two [input]
  (let [crabs (get-crabs input)]
    (calculate-moves crabs
                     (range (+ 1 (apply max (keys crabs))))
                     #(* % (/ (+ % 1) 2)))))
