(ns aoc-2021.day02
  (:gen-class)
  (:require [aoc-2021.fileio :as file] [clojure.string :as str]))

(defn split-command [cmd]
  (let [scmd (str/split cmd #" ")]
    [(first scmd) (bigint (last scmd))]))

(defn map-hpos-and-depth [[hpos depth] [dir mag]]
  (case dir
    "forward" [(+ hpos mag) depth]
    "up"      [hpos (- depth mag)]
    "down"    [hpos (+ depth mag)]))

(defn part-one [f]
  (->> (file/read-file-to-list f)
       (map split-command)
       (reduce map-hpos-and-depth [0 0])
       (apply *)))
(part-one "day_02.txt") ;; 1924923N

(defn map-hpos-aim-and-depth [[hpos depth aim] [dir mag]]
  (case dir
    "forward" [(+ hpos mag) (+ depth (* mag aim)) aim]
    "up"      [hpos depth (- aim mag)]
    "down"    [hpos depth (+ aim mag)]))

(defn part-two [f]
  (->> (file/read-file-to-list f)
       (map split-command)
       (reduce map-hpos-aim-and-depth [0 0 0])
       (take 2)
       (apply *)))
(part-two "day_02.txt") ;; 1982495697N

