(ns aoc-2021.day12
  (:gen-class)
  (:require [aoc-2021.fileio :as io]
            [clojure.string :as str]))

(defn get-pairs [name pairs]
  (filter (fn [y] (or (= name (first y)) (= name (last y)))) pairs))

(defn filter-unwanted [name pairs]
  (let [e (->> (get-pairs name pairs) (apply concat) set vec (filter #(not= name %)))]
    (if (= "start" name) (vec e)
                         (if (= "exit" name) [] (vec (filter #(not= "start" %) e))))))

(defn parse-cave-system [file]
  (let [pairs (->> (io/read-file-to-list file) (map #(str/split % #"-")))
        names (->> pairs (apply concat) set)]
    (->> names
         (map (fn [x] {:name  x :exits (filter-unwanted x pairs)}))
         (filter #(not= "end" (:name %))))))

(defn get-double-visited-small-caves [path]
  (->> (frequencies path)
       (filter #(= (str/lower-case (first %)) (first %)))
       (filter #(and (not= "start" (first %)) (not= "end" (first %))))
       (filter #(< 1 (last %)))
       count))

(defn is-small? [cave-name] (= cave-name (str/lower-case cave-name)))
(defn in-path? [path next] (some #{next} path))
(defn can-visit? [path next] (if (and (is-small? next) (in-path? path next)) false true))
(defn can-visit-extended? [path next]
  (if (not (is-small? next)) true
                             (if (= 0 (get-double-visited-small-caves path))
                               true
                               (if (in-path? path next) false true))))

(defn update-journey [path system func]
  (let [next (last path)
        cave (->> system (filter #(= (:name %) next)) first)]
    (->> (:exits cave)
         (map (fn [x] {:path (conj path x), :state (if (= "end" x) "complete"
                                                       (if (func path x) "active" "invalid"))})))))

(defn next-step [journeys system fn]
  (let [complete (filter #(= "complete" (:state %)) journeys)
        new (->> journeys
                 (filter #(= "active" (:state %)))
                 (map #(update-journey (:path %) system fn))
                 (apply concat))]
    (concat new complete)))

(defn part-one [file]
  (let [system (parse-cave-system file)]
    (loop [journeys [{:path ["start"] :state "active"}]]
      (if (nil? (some #(= "active" (:state %)) journeys))
        (count (filter #(= "complete" (:state %)) journeys))
        (recur (next-step journeys system can-visit?))))))

(defn part-two [file]
  (let [system (parse-cave-system file)]
    (loop [journeys [{:path ["start"] :state "active"}]]
      (if (nil? (some #(= "active" (:state %)) journeys))
        (count (filter #(= "complete" (:state %)) journeys))
        (recur (next-step journeys system can-visit-extended?))))))

(comment
  (part-one "day_12.txt")  ; Answer to part one - 4573
  (part-two "day_12.txt")  ; Answer to part two - 117509
  )